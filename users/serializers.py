from rest_framework import serializers

from users.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    phone = serializers.CharField(max_length=13)
    is_active = serializers.BooleanField(default=False)

    def create(self, validated_data):
        existing_user = None
        try:
            existing_user = CustomUser.objects.get(email=validated_data['email'])
        except CustomUser.DoesNotExist as e:
            print(e)

        if existing_user:
            return self.update(existing_user, validated_data)
        return CustomUser.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.save()
        return instance

    class Meta:
        model = CustomUser
        fields = '__all__'
