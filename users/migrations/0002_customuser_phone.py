# Generated by Django 2.0 on 2018-01-07 10:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='phone',
            field=models.CharField(default='+380953553540', max_length=13, verbose_name='phone'),
            preserve_default=False,
        ),
    ]
