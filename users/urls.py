from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from users import views

urlpatterns = [
    url(r'^users/$', views.UserList.as_view()),
    url(r'^users/(?P<pk>\d+)/$', views.UserList.as_view(), name='delete_event'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
